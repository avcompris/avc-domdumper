# About avc-domdumper

This is a utility framework
to help dump XML files.



This is the project home page, hosted on GitLab.


[API Documentation is here](https://maven.avcompris.com/avc-domdumper/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-domdumper/)


## Syntax

````
final File xmlFile = new File("mytest.xml");

final Dumper dumper = XMLDumpers.newDumper("myroot", xmlFile, "UTF-8");

dumper.addElement("person").addAttribute("id", "dandriana");

dumper.addElement("address")
          .addElement("city").addCharacter("Laurabuc")
              .close() 
          .addElement("zipCode").addCharacter("11400")
              .close();
````

Each time you call `addElement()`, a new
`Dumper` object is created. Either
you call `close()` to explicitly
empty the stack, or reuse your original
`Dumper` object, which will close
all sub-dumpers previously opened.

This is an API to write XML sequentially
in a file,
not to add nodes into a DOM.