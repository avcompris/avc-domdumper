package net.avcompris.domdumper;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;

import org.junit.Test;

public class XMLDumperTest {

	@Test
	public void testEmptyElement() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.close();

		assertEquals("<HelloWorld/>", bos.toString(UTF_8));
	}

	@Test
	public void testEmptyElement_indent() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", bos, UTF_8);

		dumper.close();

		assertEquals("<HelloWorld/>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_noSubClose() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi");

		dumper.close();

		assertEquals("<HelloWorld><Hi/></HelloWorld>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_characters_noSubClose() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi").addCharacters("there");

		dumper.close();

		assertEquals("<HelloWorld><Hi>there</Hi></HelloWorld>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_characters_noSubClose_indent() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", bos,
				UTF_8);

		dumper.addElement("Hi").addCharacters("there");

		dumper.close();

		assertEquals("<HelloWorld>\r\n\t<Hi>there</Hi>\r\n</HelloWorld>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_noSubClose_indent() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", bos, UTF_8);

		dumper.addElement("Hi");

		dumper.close();

		assertEquals("<HelloWorld>\r\n\t<Hi/>\r\n</HelloWorld>",
				bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_noSubClose_addElemet() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi");

		dumper.addElement("There");

		dumper.close();

		assertEquals("<HelloWorld><Hi/><There/></HelloWorld>",
				bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_subClose() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi").close();

		dumper.close();

		assertEquals("<HelloWorld><Hi/></HelloWorld>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_subClose_addElementToClose() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi").close().addElement("There");

		dumper.close();

		assertEquals("<HelloWorld><Hi/><There/></HelloWorld>",
				bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_subClose_addElement() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addElement("Hi").close();

		dumper.addElement("There");

		dumper.close();

		assertEquals("<HelloWorld><Hi/><There/></HelloWorld>",
				bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_addAttributes() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addAttribute("x", 34);
		dumper.addAttribute("y", 0);

		dumper.close();

		assertEquals("<HelloWorld x=\"34\" y=\"0\"/>", bos.toString(UTF_8));
	}

	@Test
	public void testSubElement_addAttributesChained() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers.newDumper("HelloWorld", false, bos,
				UTF_8);

		dumper.addAttribute("x", 34).addAttribute("y", 0);

		dumper.close();

		assertEquals("<HelloWorld x=\"34\" y=\"0\"/>", bos.toString(UTF_8));
	}

	@Test
	public void testNamespaces() throws Exception {

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		final Dumper dumper = XMLDumpers
				.newDumper("svg", new String[] {
						"xmlns=http://www.w3.org/2000/svg",
						"xmlns:xlink=http://www.w3.org/1999/xlink" }, false,
						bos, UTF_8);

		dumper.close();

		assertEquals("<svg xmlns=\"http://www.w3.org/2000/svg\""
				+ " xmlns:xlink=\"http://www.w3.org/1999/xlink\"/>",
				bos.toString(UTF_8));
	}
}
