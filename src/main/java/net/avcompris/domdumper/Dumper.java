package net.avcompris.domdumper;

import java.io.IOException;

import com.avcompris.common.annotation.Nullable;

public interface Dumper {

	Dumper close() throws IOException;

	Dumper addElement(String elementName, @Nullable String[] namespaces,
			Object... attributeNameValuePairs) throws IOException;

	Dumper addElement(String elementName, Object... attributeNameValuePairs)
			throws IOException;

	Dumper addAttribute(String attributeName, int attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, long attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, short attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, boolean attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, double attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, float attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, byte attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, char attributeValue)
			throws IOException;

	Dumper addAttribute(String attributeName, @Nullable String attributeValue)
			throws IOException;

	Dumper addAttributes(Object... attributeNameValuePairs) throws IOException;
	
	/**
	 * Add characters as is, that is, "&lt;", "&gt;" will not be escaped.
	 */
	Dumper addRawCharacters(String characters) throws IOException;
	
	/**
	 * Add characters, escaping the following ones:
	 * "&lt;" (&amp;lt;), "&gt;" (&amp;gt;), "&amp;" (&amp;amp;)
	 */
	Dumper addCharacters(String characters) throws IOException;
	
	/**
	 * Add characters, escaping the following ones:
	 * "&lt;" (&amp;lt;), "&gt;" (&amp;gt;), "&amp;" (&amp;amp;)
	 */
	Dumper addCharacters(char c) throws IOException;
}
