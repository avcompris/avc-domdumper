package net.avcompris.domdumper;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.NotImplementedException;

import com.avcompris.common.annotation.Nullable;

public abstract class XMLDumpers {

	public static Dumper newDumper(final String rootElementName,
			final File outputFile, final String encoding) throws IOException {

		return newDumper(rootElementName, true, outputFile, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			final boolean indent, final File outputFile, final String encoding)
			throws IOException {

		return newDumper(rootElementName, null, true, outputFile, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			@Nullable final String[] namespaces, final boolean indent,
			final File outputFile, final String encoding) throws IOException {

		return newDumper(rootElementName, namespaces, null, true, outputFile,
				encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			@Nullable final String[] namespaces,
			@Nullable final Object[] attributeNameValuePairs,
			final boolean indent, final File outputFile, final String encoding)
			throws IOException {

		return new XMLDumper(rootElementName, namespaces,
				attributeNameValuePairs, indent, outputFile, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			final OutputStream os, final String encoding) throws IOException {

		return newDumper(rootElementName, true, os, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			final boolean indent, final OutputStream os, final String encoding)
			throws IOException {

		return newDumper(rootElementName, null, indent, os, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			@Nullable final String[] namespaces, final OutputStream os,
			final String encoding) throws IOException {

		return newDumper(rootElementName, namespaces, false, os, encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			@Nullable final String[] namespaces, final boolean indent,
			final OutputStream os, final String encoding) throws IOException {

		return newDumper(rootElementName, namespaces, null, indent, os,
				encoding);
	}

	public static Dumper newDumper(final String rootElementName,
			@Nullable final String[] namespaces,
			@Nullable final Object[] attributeNameValuePairs,
			final boolean indent, final OutputStream os, final String encoding)
			throws IOException {

		return new XMLDumper(rootElementName, namespaces,
				attributeNameValuePairs, indent, os, encoding);
	}

	private static class XMLDumper extends AbstractDumperImpl {

		public XMLDumper(final String rootElementName,
				@Nullable final String[] namespaces,
				@Nullable final Object[] attributeNameValuePairs,
				final boolean indent, final File outputFile,
				final String encoding) throws IOException {

			super(rootElementName, namespaces, attributeNameValuePairs, indent);

			checkNotNull(outputFile, "outputFile");
			checkNotNull(encoding, "encoding");

			os = new FileOutputStream(outputFile);

			try {

				writer = new OutputStreamWriter(os, encoding);

				init(this, writer);

			} catch (final IOException e) {

				os.close();

				throw e;
			}
		}

		public XMLDumper(final String rootElementName,
				@Nullable final String[] namespaces,
				@Nullable final Object[] attributeNameValuePairs,
				final boolean indent, final OutputStream os,
				final String encoding) throws IOException {

			super(rootElementName, namespaces, attributeNameValuePairs, indent);

			checkNotNull(os, "os");
			checkNotNull(encoding, "encoding");

			this.os = null;

			writer = new OutputStreamWriter(os, encoding);

			try {

				init(this, writer);

			} catch (final IOException e) {

				writer.close();

				throw e;
			}
		}

		private final Writer writer;

		@Nullable
		private final OutputStream os;

		@Override
		public final Dumper close() throws IOException {

			final Dumper dumper;

			if (os == null) {

				try {

					dumper = super.close();

					writer.flush();

				} finally {

					writer.close();
				}

			} else {

				try {

					dumper = super.close();

					writer.flush();

				} finally {

					os.close();
				}
			}

			return dumper;
		}
	}

	private static abstract class AbstractDumperImpl implements Dumper {

		protected AbstractDumperImpl(final String elementName,
				@Nullable final String[] namespaces,
				@Nullable final Object[] attributeNameValuePairs,
				final boolean indent) {

			this.elementName = checkNotNull(elementName, "elementName");
			this.namespaces = namespaces;
			this.attributeNameValuePairs = attributeNameValuePairs;
			this.indent = indent;
		}

		private final String elementName;
		@Nullable
		private final String[] namespaces;
		@Nullable
		private final Object[] attributeNameValuePairs;
		protected final boolean indent;
		protected int level = 0;

		private Writer writer;

		protected final void init(final AbstractDumperImpl parentDumper,
				final Writer writer) throws IOException {

			if (this.writer != null) {
				throw new IllegalStateException(
						"AbstractDumperImpl#init(Writer) cannot be called twice.");
			}

			this.parentDumper = checkNotNull(parentDumper, "parentDumper");

			if (parentDumper != this) {

				this.level = parentDumper.level + 1;
			}

			this.writer = checkNotNull(writer);

			if (indent) {
				writer.write(repeat('\t', level));
			}

			writer.write('<');
			writer.write(elementName);

			if (namespaces != null && namespaces.length != 0) {
				for (final String namespace : namespaces) {
					if (namespace == null) {
						continue;
					}
					if (!namespace.contains("=")
							|| !namespace.startsWith("xmlns")) {
						throw new IllegalArgumentException(
								"namespace should be of the form \"xmlns[:xxx]=yyy\", but was: "
										+ namespace);
					}
					final String left = substringBefore(namespace, "=");
					final String right = substringAfter(namespace, "=");
					writer.write(' ');
					writer.write(left);
					writer.write('=');
					if (right.startsWith("'") || right.startsWith("\"")) {
						writer.write(right);
					} else {
						writer.write('"');
						writer.write(right);
						writer.write('"');
					}
				}
			}

			if (attributeNameValuePairs != null
					&& attributeNameValuePairs.length != 0) {

				addAttributes(attributeNameValuePairs);
			}
		}

		private Dumper parentDumper;

		private void assertInited() {

			if (this.writer == null) {
				throw new IllegalStateException(
						"AbstractDumperImpl#init() should have been called first.");
			}
		}

		private boolean textHasBeenInserted = false;

		@Override
		public Dumper close() throws IOException {

			assertInited();

			if (isClosed) {
				return parentDumper;
			}

			if (inTag) {

				writer.write("/>");

				inTag = false;

			} else {

				while (!lastSubDumpers.isEmpty()) {

					final SubDumper dumper = lastSubDumpers.pop();

					dumper.close();
				}

				if (indent && !textHasBeenInserted) {
					writer.write(repeat('\t', level));
				}

				writer.write("</");
				writer.write(elementName);
				writer.write('>');
			}

			if (indent && level != 0) {
				writer.write("\r\n");
			}

			isClosed = true;

			return parentDumper;
		}

		private boolean inTag = true;

		private boolean isClosed = false;

		@Override
		public final Dumper addElement(final String elementName,
				final Object... attributeNameValuePairs) throws IOException {

			return addElement(elementName, null, attributeNameValuePairs);
		}

		private void ensureTagIsClosed(final boolean followingIsAnElement)
				throws IOException {

			assertInited();

			if (isClosed) {
				throw new IllegalStateException(
						"Dumper has already been closed.");
			}

			if (inTag) {

				writer.write('>');

				inTag = false;

				if (indent && followingIsAnElement) {
					writer.write("\r\n");
				}

			} else {

				while (!lastSubDumpers.isEmpty()) {

					final SubDumper dumper = lastSubDumpers.pop();

					dumper.close();
				}
			}
		}

		@Override
		public final Dumper addElement(final String elementName,
				@Nullable final String[] namespaces,
				final Object... attributeNameValuePairs) throws IOException {

			ensureTagIsClosed(true);

			final SubDumper dumper = new SubDumper(writer, this, elementName,
					namespaces, attributeNameValuePairs);

			lastSubDumpers.push(dumper);

			textHasBeenInserted = false;

			return dumper;
		}

		@Override
		public final Dumper addRawCharacters(final String text)
				throws IOException {

			checkNotNull(text, "text");

			ensureTagIsClosed(false);

			writer.write(text);

			textHasBeenInserted = (text.length() != 0);

			return this;
		}

		@Override
		public final Dumper addCharacters(final String text) throws IOException {

			checkNotNull(text, "text");

			ensureTagIsClosed(false);

			writer.write(escapeXML_dontEscapeQuote(text));

			textHasBeenInserted = (text.length() != 0);

			return this;
		}

		@Override
		public final Dumper addCharacters(final char c) throws IOException {

			return addCharacters(Character.toString(c));
		}

		private final Stack<SubDumper> lastSubDumpers = new Stack<SubDumper>();

		private final Set<String> attributeNames = new HashSet<String>();

		@Override
		public final Dumper addAttribute(final String attributeName,
				final int attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Integer.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final long attributeValue) throws IOException {

			return addRawAttribute(attributeName, Long.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final short attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Short.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final float attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Float.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final double attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Double.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final byte attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Integer.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final char attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Character.toString(attributeValue));
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				final boolean attributeValue) throws IOException {

			return addRawAttribute(attributeName,
					Boolean.toString(attributeValue));
		}

		private final Dumper addRawAttribute(final String attributeName,
				final String attributeRawValue) throws IOException {

			checkNotNull(attributeName, "attributeName");

			assertInited();

			if (attributeNames.contains(attributeName)) {
				throw new IllegalStateException("Duplicate attributeName: "
						+ attributeName);
			}

			attributeNames.add(attributeName);

			writer.write(' ');
			writer.write(attributeName);
			writer.write("=\"");
			writer.write(attributeRawValue);
			writer.write('"');

			return this;
		}

		@Override
		public final Dumper addAttribute(final String attributeName,
				@Nullable final String attributeValue) throws IOException {

			checkNotNull(attributeName, "attributeName");

			assertInited();

			if (attributeValue == null) {
				return this;
			}

			if (attributeNames.contains(attributeName)) {
				throw new IllegalStateException("Duplicate attributeName: "
						+ attributeName);
			}

			attributeNames.add(attributeName);

			writer.write(' ');
			writer.write(attributeName);
			writer.write("=\"");
			writer.write(escapeXML_alsoEscapeQuote(attributeValue));
			writer.write('"');

			return this;
		}

		@Override
		public final Dumper addAttributes(
				@Nullable final Object... attributeNameValuePairs)
				throws IOException {

			assertInited();

			if (attributeNameValuePairs == null
					|| attributeNameValuePairs.length == 0) {
				return this;
			}

			if (attributeNameValuePairs.length % 2 != 0) {
				throw new IllegalArgumentException(
						"attributeNameValuePairs.length should be an even number, but was: "
								+ attributeNameValuePairs.length);
			}

			for (int i = 0; i < attributeNameValuePairs.length; ++i) {

				final Object attributeName = attributeNameValuePairs[i];

				if (attributeName == null) {
					throw new IllegalArgumentException(
							"attributeName should not be null");
				}

				if (!(attributeName instanceof String)) {
					throw new IllegalArgumentException(
							"attributeName should be a String, but was: "
									+ attributeName.getClass().getName() + " ("
									+ attributeName + ")");
				}

				++i;

				final Object attributeValue = attributeNameValuePairs[i];

				if (attributeValue == null) {
					continue;
				}

				if (attributeValue instanceof Integer) {
					addAttribute(attributeName.toString(),
							((Integer) attributeValue).intValue());
				} else if (attributeValue instanceof Long) {
					addAttribute(attributeName.toString(),
							((Long) attributeValue).longValue());
				} else if (attributeValue instanceof Boolean) {
					addAttribute(attributeName.toString(),
							((Boolean) attributeValue).booleanValue());
				} else if (attributeValue instanceof String) {
					addAttribute(attributeName.toString(),
							(String) attributeValue);
				} else {
					throw new NotImplementedException("attributeValue.type: "
							+ attributeValue.getClass().getName() + " ("
							+ attributeValue + ")");
				}
			}

			return this;
		}
	}

	private static class SubDumper extends AbstractDumperImpl {

		public SubDumper(final Writer writer,
				final AbstractDumperImpl parentDumper,
				final String elementName, @Nullable final String[] namespaces,
				@Nullable final Object[] attributeNameValuePairs)
				throws IOException {

			super(elementName, namespaces, attributeNameValuePairs,
					parentDumper.indent);

			init(parentDumper, writer);
		}
	}

	public static String escapeXML_alsoEscapeQuote(final String value) {

		checkNotNull(value, "value");

		return value.replace("&", "&amp;").replace("<", "&lt;")
				.replace(">", "&gt;").replace("\"", "&quot;");
	}

	public static String escapeXML_dontEscapeQuote(final String value) {

		checkNotNull(value, "value");

		return value.replace("&", "&amp;").replace("<", "&lt;")
				.replace(">", "&gt;");
	}
}
